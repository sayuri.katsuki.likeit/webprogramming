<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>userlist</title>

     <style type="text/css">

  p {
      color: white;
      font-size:12pt;
      background-color:DimGray
         }

         a.sample1{
             color:red;
             text-decoration: underline
             }

         a.sample2{
             text-decoration: underline
             }
}

</style>

    <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
</head>
	<body>
     <div align="right">
         <p><b>${sessionScope.userInfo.name} さん</b> <a href="LogoutServlet" class="sample1">ログアウト</a> </p> </div>
         <h1 align="center"><b>ユーザー一覧</h1></b>
        <br>
        <div align="right"><a href="UserRegisterServlet" class="sample2">新規登録</a>&emsp;&emsp;</div>
<div style="text-align:center">    　


                                        　　
<form action="UserListServlet" method="post">
<b>ログインId</b>&thinsp;&thinsp;&emsp;<input type="text"  size="49" name="loginid">
    <br>
    <br>
<b>ユーザー名</b>&thinsp;&thinsp;&emsp;<input type="text"  size="49" name="name">
     <br>
     <br>
     <b>生年月日</b>&thinsp;&thinsp;&emsp;&emsp;<input type="date" name="birthday">&emsp;～&emsp;&emsp;<input type="date" name="birthday2">
     <br>
     <br>
<div align="right"><input type="submit" class="btn btn-outline-dark" value="　検索&hearts;　">&emsp;&emsp;</div></form>
<br>

<hr width="90%" align="center">
     <table class="table table-sm  table-bordered">

  <thead class="thead-light">
      <tr>
      <th scope="col"><b>ログインID</b></th>
      <th scope="col"><b>ユーザー名</b></th>
      <th scope="col"><b>生年月日</b></th>
      <th scope="col"></th>

    </tr>
  </thead>
  <tbody>

  <c:choose>
  <c:when test="${findlist != null}">
    <c:forEach var="findlist" items="${findlist}" >
    <tr>
     <td>${findlist.loginId}</td>
      <td>${findlist.name}</td>
      <td>${findlist.birthDate}</td>
      <td>&thinsp;<a class="btn btn-primary  btn-sm" href="UserDetailServlet?id=${findlist.id}" role="button">詳細</a>
          &nbsp;<c:if test ="${findlist.id == idCompare.id or idCompare.id == '10'}"><a class="btn btn-info btn-sm" href="UserUpdateServlet?id=${findlist.id}" role="button">更新</a></c:if>
          &nbsp;<c:if test ="${idCompare.id == '10'}"><a class="btn btn-danger btn-sm" href="UserDeleteServlet?id=${findlist.id}" role="button">削除</a></c:if></td>
    </tr>

      </c:forEach>
  </c:when>
  <c:otherwise>
       <c:forEach var="userList" items="${sessionScope.userList}" >
    <tr>
     <td>${userList.loginId}</td>
      <td>${userList.name}</td>
      <td>${userList.birthDate}</td>
      <td>&thinsp;<a class="btn btn-primary  btn-sm" href="UserDetailServlet?id=${userList.id}" role="button">詳細</a>
          &nbsp;<c:if test ="${userList.id == idCompare.id or idCompare.id == '10'}"><a class="btn btn-info btn-sm" href="UserUpdateServlet?id=${userList.id}" role="button">更新</a></c:if>
          &nbsp;<c:if test ="${idCompare.id == '10'}"><a class="btn btn-danger btn-sm" href="UserDeleteServlet?id=${userList.id}" role="button">削除</a></c:if></td>
    </tr>

      </c:forEach>
  </c:otherwise>
</c:choose>
  </tbody>
</table>



</div>
	</body>
</html>