package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
	    User u = (User)session.getAttribute("userInfo");

		if(u == null){
			response.sendRedirect("LoginServlet");
			return;
		}
		String id = request.getParameter("id");

		// 確認用：idをコンソールに出力
		System.out.println(id);

		// ユーザ一覧情報を取得
		UserDao userDao = new UserDao();
		User user = userDao.Detail(id);

		// TODO  未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード

		request.setAttribute("user", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Mock/update.jsp");
		dispatcher.forward(request, response);


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        // リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");


        String password = request.getParameter("password");
        String password2 = request.getParameter("password2");
        String name = request.getParameter("name");
        String birthDay = request.getParameter("birthDate");

        String loginId = request.getParameter("loginId");
        System.out.println("loginId:"+loginId+" password:"+password+"password2"+password2+" name:"+name+" birthDay:"+birthDay);

        UserDao userDao = new UserDao();
System.out.println("aaa2");

        if(name.equals("")||birthDay.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Mock/update.jsp");
			dispatcher.forward(request, response);


   	 }else if(password.equals("")||password2.equals("")) {

    	 userDao.UpdateNopassword(loginId,name,birthDay);
    		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Mock/list.jsp");
    		dispatcher.forward(request, response);

	 }else if(!password.equals(password2)) {
		request.setAttribute("errMsg", "入力された内容は正しくありません");
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Mock/update.jsp");
		dispatcher.forward(request, response);

	 }else {

        userDao.Update(loginId,password,name,birthDay);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Mock/list.jsp");
		dispatcher.forward(request, response);
	}
}
}