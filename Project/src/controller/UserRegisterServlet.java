package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserRegisterServlet
 */
@WebServlet("/UserRegisterServlet")
public class UserRegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserRegisterServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
	    User u = (User)session.getAttribute("userInfo");

		if(u == null){
			response.sendRedirect("LoginServlet");
			return;
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Mock/register.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String username = request.getParameter("username");
		String birthday = request.getParameter("birthday");



//		System.out.println(loginId);
//		System.out.println(password);
//		System.out.println(password2);
//		System.out.println(username);
//		System.out.println(birthday);
//
		UserDao userDao = new UserDao();
		String check = userDao.Check(loginId);

//		ろぐいんIDさえ入力すればとおりぬけてしまう→NULL判定の文がおかしくすりぬけてる説
//		登録DAOに戻り値つけてそれがNULLかどうか判定すればいいのでは　DAOがうまくいかず
//		ビーンズに入れてNULL判定　よくわかけあん


////

         if(check!=null) {
        		request.setAttribute("errMsg", "入力された内容は正しくありません");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Mock/register.jsp");
				dispatcher.forward(request, response);
				return;

	     }else if(loginId.equals("")||password.equals("")||password2.equals("")||username.equals("")||birthday.equals("")) {
				request.setAttribute("errMsg", "入力された内容は正しくありません");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Mock/register.jsp");
				dispatcher.forward(request, response);
				return;

		 }else if(!password.equals(password2)) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Mock/register.jsp");
			dispatcher.forward(request, response);
			return;

		 }else {

	     userDao.Register(loginId, password, username, birthday);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Mock/list.jsp");
		dispatcher.forward(request, response);
				}
	}

}
