package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession();
	    User u = (User)session.getAttribute("userInfo");

		if(u == null){
			response.sendRedirect("LoginServlet");
			return;
		}
		String id = request.getParameter("id");

		// 確認用：idをコンソールに出力
		System.out.println(id);

		// ユーザ一覧情報を取得
		UserDao userDao = new UserDao();
		User user = userDao.Detail(id);

		// TODO  未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード

		request.setAttribute("user", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Mock/delete.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub


        // リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        String id = request.getParameter("id");


		// 確認用：idをコンソールに出力
		System.out.println(id);

		// TODO  未実装：idを引数にして、idに紐づくユーザ情報を出力する
		UserDao userDao = new UserDao();
		userDao.Delete(id);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Mock/list.jsp");
		dispatcher.forward(request, response);


		//
//		・選択されたユーザに紐づくデータを、「ユーザ情報テーブル」から削除する。
//
	}

}
