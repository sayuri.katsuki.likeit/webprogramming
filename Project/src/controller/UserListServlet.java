package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserListServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO 未実装：ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
	    User u = (User)session.getAttribute("userInfo");

		if(u == null){
			response.sendRedirect("LoginServlet");
			return;
		}

//		この合言葉は後回しできるっけ？
//		→資料によると、
//		セッションスコープはリクエストをまたいでデータを保持し続けることができるため、
//		一度サーブレットでセットしたデータを別のサーブレットで呼び出すことがあります。らしい。つまりできる。

//
//		HttpSession session = request.getSession();
//	    User u = (User)session.getAttribute("userInfo");
//
//		if(u = null){
//			response.sendRedirect("LoginServlet");
//		}


		// ユーザ一覧情報を取得
		UserDao userDao = new UserDao();
		List<User> userList =userDao.findAll();



		// リクエストスコープにユーザ一覧情報をセット
		HttpSession session1 = request.getSession();
		session1.setAttribute("userList", userList);

		// ユーザ一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Mock/list.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO  未実装：検索処理全般
	       // リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("loginid");
		String name = request.getParameter("name");
		String birthday = request.getParameter("birthday");
		String birthday2 = request.getParameter("birthday2");

		System.out.println("Id"+loginId+"name"+name+"b1"+birthday+"b2"+birthday2);

		UserDao userDao = new UserDao();

	    List<User> findlist =userDao.findUser(loginId,name,birthday,birthday2);



	    request.setAttribute("findlist",findlist);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Mock/list.jsp");
		dispatcher.forward(request, response);




	}
}


